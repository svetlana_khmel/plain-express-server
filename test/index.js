'use strict';
process.env.NODE_ENV = 'test';

var nock = require('nock');
var test = require('tape');
var request = require('supertest');
var app = require('../app');
const expect = require('chai').expect;
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();

var Merchant = require("../models/merchants");

chai.use(chaiHttp);

const response = [ { bids: [ { id: '4cf99800-c81e-11e8-8974-037e81ba72ba', carTitle: 'we', amount: '12', created: 1538689310080 } ], _id: '5bb683ce4224d42156fa79fa', firstname: '21', lastname: '2', __v: 0 }, { bids: [ { id: 'f3baa1d0-cb10-11e8-82b7-23d4aed0fecd', carTitle: 'qwe', amount: '12', created: 1539013430381 } ], _id: '5bbb7b2ac925ca49bd2295de', firstname: '1', lastname: '1', __v: 0 } ]
const data = { bids: [ { id: '4cf99800-c81e-11e8-8974-037e81ba72ba', carTitle: 'we', amount: '12', created: 1538689310080 } ], _id: '5bb683ce4224d42156fa79fa', firstname: '21', lastname: '2', __v: 0 }

describe('First test', () => {
  it('Should assert true to be true', () => {
    expect(true).to.be.true;
  });
});

describe('Мerchants ', () => {
  Merchant.collection.drop();

  beforeEach(function(done){
    var newMerchant = new Merchant(data);
    newMerchant.save(function(err) {
      done();
    });
  });
  afterEach(function(done){
    Merchant.collection.drop();
    done();
  });

  it('should list ALL merchants on /api/merchants GET', (done) => {
    request(app)
      .get('/api/merchants')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(function (err, res) {
        expect(res.body).to.have.lengthOf(1);
        expect(res.body).to.be.a('array');
        done()
      });
  });

  it('should add a SINGLE merchant on /api/merchants POST', function(done) {
    chai.request(app)
      .post('/api/merchants')
      .send(data)
      .end(function(err, res){
        res.should.have.status(200);
        res.should.be.json;
        //res.body.should.be.a('array');
        //res.body.should.be.a('object')
        //res.should.have.property('firstname:');
        done();
      });
  });

  it('should update a SINGLE merchant on /api/merchants<id> PUT', function(done) {
    chai.request(app)
      .get('/api/merchants')
      .end(function(err, res){
        chai.request(app)
          .put('/api/merchants/'+res.body[0]._id)
          .send(data)
          .end(function(error, response){
            response.should.have.status(200);
            response.should.be.json;
            response.body.should.be.a('object');
            done();
          });
      });
  });

  it('should delete a SINGLE merchant on /api/merchants<id> DELETE', function(done) {
    chai.request(app)
      .get('/api/merchants')
      .end(function(err, res){
        chai.request(app)
          .delete('/api/merchants/'+res.body[0]._id)
          .end(function(error, response){
            response.should.have.status(200);
            response.should.be.json;
            response.body.should.be.a('object');
            done();
          });
      });
  });
})

// describe('Get merchants tests', () => {
//   beforeEach(() => {
//     nock('http://localhost:3300')
//       .get('/api/merchants')
//       .reply(200, response);
//   });
//
//   it('GET:: Correct merchants returned', (done) => {
//       request(app)
//     .get('/api/merchants')
//     .expect('Content-Type', /json/)
//     .expect(200)
//     .end(function (err, res) {
//       expect(res.body).to.have.lengthOf(2);
//       expect(res.body).to.be.a('array');
//       done()
//     });
//   });
// });




