const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;

const bidSchema = new Schema({
  carTitle: Schema.Types.Mixed,
  amount: Number,
  created: Schema.Types.Mixed
});

module.exports = mongoose.model('Bid', bidSchema);


