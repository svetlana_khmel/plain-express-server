const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;

const merchantSchema = new Schema({
  firstname: {
    type: String,
   // validate: [nameValidator, '{VALUE} name should start from capital letter!'],
  },
  lastname: {
    type: String,
   // validate: [nameValidator, '{VALUE} name should start from capital letter!'],
  },
  avatarUrl: String,
  email: String,
  phone: String,
  hasPremium: Boolean,
  country: String,
  bids: []
  //bids: {type: Schema.Types.ObjectId, ref: 'Bids'}
});

module.exports = mongoose.model('Merchant', merchantSchema);
//
// function nameValidator (value) {
//   return /^[A-Z]/.test(value);
// }


