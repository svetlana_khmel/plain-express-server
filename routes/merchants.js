const express = require('express');
let router = express.Router();
const Merchants = require('../models/merchants');

router.get('/', (req, res, next) => {
  Merchants.find({}, (err, merchants) => {
    if (err) {
      return next(err);
    }
    res.json(merchants);
  })
});

router.post('/', (req, res, next) => {
  const data = req.body;
  console.log("POST DATA: ", data);
  if (Object.keys(data) == 0) return;
  let merchant = new Merchants(
    data
  );

  merchant.save((err, merchant) => {
    console.log("ITEM HAS BEEN ADDED:  ", merchant);
    if(err) {
      res.json({'ERROR': err});
    } else {
      res.json(merchant);
    }
  });
});

router.put('/:id', (req, res, next) => {
  const data = req.body;
  Merchants.update({'_id': req.params.id}, data, (err, merchant) => {
    console.log("ITEM HAS BEEN UPDATED:  ", merchant);
    if(err) {
      res.json({'ERROR': err});
    } else {
      res.json(merchant);
    }
  });
})

router.delete('/:id', (req, res, next) => {
  console.log("req.params.id: ", req.params.id);
  Merchants.findOneAndDelete({'_id':req.params.id}, (err, merchant) => {
    if (err) return next(err);
    console.log("ITEM HAS BEEN DELETED:  ", merchant);
    if(err) {
      res.json({'ERROR': err});
    } else {
      res.json(merchant);
    }
  })
});

module.exports = router;