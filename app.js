const express = require('express');
const app = express();
const path = require('path');
const mongoose = require('mongoose');
const merchants = require('./routes/merchants');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const config = require('./config');

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-Width, Content-Type, Accept");
  res.header('Access-Control-Allow-Headers: X-Requested-With');
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  next();
})

//app.use(bodyParser.urlencoded({ extended: false }))
// app.get('/', function (req, res) {
//   res.sendFile(path.join(__dirname, './data/data.json'));
// })

// parse application/json
app.use(bodyParser.json())

const dbUrl = config[app.settings.env];
console.log('.... app.settings.env .... ', app.settings.env);
app.use('/api/merchants', merchants);


console.log("DB URL.... ", dbUrl);
mongoose.connect(dbUrl).then(
  () => {
    console.log(`MongoDB is connected.`);
  },
  err => {
    console.log(`Mongoose Connection ERROR: ${err}`);
  }
);

app.listen(3300)

module.exports = app;