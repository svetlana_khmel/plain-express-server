## Building a Node.js REST API 8: Unit Testing ##

- https://medium.com/@xoor/building-a-node-js-rest-api-8-unit-testing-822c32a587df

- https://github.com/mpayetta/node-es6-rest-api/blob/master/server/test/routes/tasks.test.js

https://www.npmjs.com/package/supertest-as-promised

- https://www.codementor.io/knownasilya/testing-express-apis-with-supertest-du107mcv2

- https://www.alexjamesbrown.com/blog/development/stubbing-middleware-testing-express-supertest/

- https://tobythetesterblog.wordpress.com/2016/05/01/mocking-a-restful-api-using-nock-supertest-mocha-and-chai/

- http://developmentnow.com/2015/02/05/make-your-node-js-api-bulletproof-how-to-test-with-mocha-chai-and-supertest/

- https://www.chaijs.com/guide/styles/

- !!!! https://mherman.org/blog/testing-node-js-with-mocha-and-chai/

- https://mherman.org/blog/testing-node-js-with-mocha-and-chai/
```angular2html
  "scripts": {
    "test": "node test | tap-spec",
    "start": "node app.js"
  },
```