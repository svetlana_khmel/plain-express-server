const config = {
  'development': 'mongodb://localhost:27017/myapp',
  'test': 'mongodb://localhost/node-test'
}

module.exports = config;